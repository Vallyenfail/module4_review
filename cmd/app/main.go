package main

import (
	"context"
	"fmt"
	"gitlab.com/Vallyenfail/module4_review/internal/models"
	"gitlab.com/Vallyenfail/module4_review/internal/repository"
	"time"
)

type Repo struct {
	repo repository.TestRepository
}

func NewRepo(db repository.TestRepository) *Repo {
	return &Repo{repo: db}
}

func main() {
	db := repository.NewRepository()

	repo := NewRepo(db)

	var ctx context.Context

	err := repo.repo.Create(ctx, "Moscow", "Best")
	if err != nil {
		fmt.Println(err)
	}

	time.Sleep(time.Second * 10)

	city := models.City{
		Name:  "St.Petersburg",
		State: "Less best",
	}

	err = repo.repo.Update(ctx, city, 1)
	if err != nil {
		fmt.Println(err)
	}

	time.Sleep(time.Second * 10)

	cities := repo.repo.List(ctx)

	fmt.Println(cities)

	err = repo.repo.Delete(ctx, 1)
	if err != nil {
		fmt.Println(err)
	}
}
