package models

type City struct {
	Name  string `db:"name"`
	State string `db:"state"`
}
