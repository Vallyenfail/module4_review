package repository

import (
	"context"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/Vallyenfail/module4_review/internal/models"
	"log"
)

type TestRepository interface {
	Create(ctx context.Context, name string, state string) error
	Update(ctx context.Context, city models.City, id int) error
	Delete(ctx context.Context, id int) error
	List(ctx context.Context) []models.City
}

type Repo struct {
	db *sqlx.DB
}

func NewRepository() *Repo {
	dsn := "host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable"
	driver := "postgres"
	sqlDB, err := sqlx.Connect(driver, dsn)

	if err != nil {
		log.Println(err)
		return &Repo{}
	}

	schema := `CREATE TABLE IF NOT EXISTS cities (
    id integer NOT NULL PRIMARY KEY,
    name varchar(30) NOT NULL,
    state varchar(30) NOT NULL);`

	_, err = sqlDB.Exec(schema)

	if err != nil {
		log.Println(err)
	}

	return &Repo{db: sqlDB}
}

func (r *Repo) Create(ctx context.Context, name string, state string) error {
	_, err := r.db.ExecContext(ctx, "INSERT INTO cities (name, state) VALUES ($1, $2)", name, state)
	return err
}

func (r *Repo) Update(ctx context.Context, city models.City, id int) error {
	_, err := r.db.ExecContext(ctx, `UPDATE cities SET name=$2, state=$3 WHERE id=$1`, id, city.Name, city.State)
	return err
}

func (r *Repo) Delete(ctx context.Context, id int) error {
	_, err := r.db.ExecContext(ctx, `DELETE FROM cities WHERE id=$1`, id)
	return err
}

func (r *Repo) List(ctx context.Context) []models.City {
	var cities []models.City

	err := r.db.SelectContext(ctx, &cities, `SELECT id, name, state FROM cities`)
	if err != nil {
		log.Println(err)
	}
	return cities
}
